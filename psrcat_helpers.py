#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 25 09:41:33 2018

@author: shaifullah
"""

import os
import subprocess
import logging
from file_helpers import  download_file

logger = logging.getLogger('psrcat_helpers')

def fetchbuild_psrcat():
    """
    Fetch the psrcat source code and build it if it doesn't exist on the host.

    Returns
    -------
    object
    """
    owd = os.getcwd()
    try:
        if not os.path.exists("./psrcat_tar"):
            download_file(filename='psrcat_pkg.tar.gz', path='/people/pulsar/psrcat/downloads/',
                          url='http://www.atnf.csiro.au')
            os.system('tar -xvf psrcat_pkg.tar.gz')
            os.chdir('./psrcat_tar')
            os.system('/bin/bash makeit')
            logging.info('Build step succeeded for PSRCAT.')
    except Exception as e:
        logging.error('''
                      You requested a coordinate lookup via PSRCAT but no psrcat path was supplied 
                      and I failed to download and/or build PSRCAT in the current directory. Exiting now.
                      Please supply path to psrcat or download and build it manually from :
                      https://www.atnf.csiro.au/research/pulsar/psrcat/downloads/psrcat_pkg.tar.gz
                      ''')
        logging.exception(e)
        raise SystemExit(1)
    os.chdir(owd)


def check_psrcat_exists(psrcat_path, build=False):
    """
    Check if psrcat exists, if not download and build it in the current directory.
    IF it exists, then query psrcat for the source coordinates if they were not supplied on the command line.
    """
    psrcat_call = str(psrcat_path) + ' -v'
    child = subprocess.Popen(psrcat_call, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    child.wait()
    psrcat_status = child.returncode
    logging.info(psrcat_path, psrcat_call, psrcat_status)
    if not (psrcat_status == 1) and (build == True):
        fetchbuild_psrcat()


def populate_from_psrcat(source_name, psrcat_path):
    """
    Populate the source coordinates and velocity from psrcat

    Parameters
    ----------
    source_name : Pulsar for which coordinates are being queried
    psrcat_path : path to find PSRCAT on. If nothing is supplied, PSRCAT is downloaded and
    built in the current working directory.

    Returns
    -------
    gl : Galactic latitude (J2000)
    gb : Galactic longitude (J2000)
    dist :  Distance to pulsar (kpc)
    pml :  Proper motion in latitude
    pmb :  Proper motion in longitude
    pepoch : The epoch at which the position was determined.

    """
    check_psrcat_exists(psrcat_path)
    psrcat_call = [str(psrcat_path), '-nonumber', '-nohead',
                   '-o', 'long_error',
                   '-null', '0.0',
                   '-c', "jname gl gb dist pepoch pml pmb",
                   source_name]
    try:
        jname, jnref, \
        gl, \
        gb, \
        dist, \
        pepoch, pepochref, \
        pml, pmlerr, \
        pmb, pmberr = subprocess.check_output(psrcat_call).split()
        logging.info('Pulsar coordinates obtained from psrcat sucessfully!')

        assert jname == source_name

    except Exception as e:
        logging.error('Could not get pulsar info from PSRCAT. Check your installation or (re)run install.py')
        logging.exception(e)
        raise SystemExit(1)

    return gl, gb, dist, pml, pmb, pepoch


def main():
    """
    Module to help download and build psrcat

    Returns
    -------

    """
    logging.info("""This is a module that looks for psrcat. If it does not find it on the system path, it downloads \n
                 and builds the latest version of psrcat. Psrcat is queried for pulsar source coordinates.""")
    import argparse

    parser = argparse.ArgumentParser(description="""Module to download and build psrcat. 
                                                    Also provides a convenience function to query psrcat.""")
    parser.add_argument('--install', default=False, type=bool,action='store_true')
    parser.add_argument('--check', default=False, type=bool,action='store_true')
    args = parser.parse_args()
    if args.install:
        fetchbuild_psrcat()
    if args.check:
        check_psrcat_exists(build=True)

if __name__ == "__main__":
    main()
