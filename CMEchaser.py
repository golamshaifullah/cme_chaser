#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 25 09:41:33 2018

@author: shaifullah
"""

import matplotlib

matplotlib.use('qt5agg')

import os
import argparse
import subprocess
import math
import numpy as np
import pandas as pd
import pycurl
import logging

from astropy.coordinates import SkyCoord, Angle
from matplotlib.collections import PathCollection
from astropy.time import Time
from astroquery.simbad import Simbad
from astroquery.vizier import Vizier
from matplotlib.image import imread
from matplotlib.transforms import Affine2D
from scipy.io.idl import readsav
from sunpy.coordinates.ephemeris import get_horizons_coord
from sunpy.coordinates import sun as get_sun
from sunpy.net.helioviewer import HelioviewerClient
import astropy.units as u
import matplotlib.pyplot as plt
import sunpy.map as sunpy_map

logger = logging.getLogger('CMEchaser')

# Define constants for use later
_VOLCOEFF = 4. / 3. * np.pi
_CM2AU = 149.6e6 * 1e5
_AU2PC = 3.086e13 / 1.496e8
_PC2AU = 1.496e8 / 3.086e13
_MP = 1.67e-27  # *u.kg
_RSOL_AU = 0.00464913034  # !/usr/bin/env python3


# CME physics functions
def leblanc_proton_density(R):
    """

    Parameters
    ----------
    R

    Returns
    -------

    """
    Ni_R = 3.3e5 * np.power(R, -2.) + 4.1e6 * np.power(R, -4) + 8e7 * np.power(R, -6)  # cm−3
    return Ni_R


def expansion_velocity_gdya09(width=90., velocity=150.):
    """
    # CME expansion speed relation from
    # Gopalaswamy et al., (2009), Cent. Eur. Astrophys. Bull.

    Parameters
    ----------
    width
    velocity

    Returns
    -------
    expansuin_velocity_final
    """
    expansion_velocity_final = velocity / (0.5 * (1 + np.cotdg(width / 2.)))
    return expansion_velocity_final


def propagation_time(velocity, mdlos, accel):
    """

    Parameters
    ----------
    velocity
    mdlos
    accel

    Returns
    -------

    """
    km2au = 149.6e6
    logging.debug('Minimimum distance to LoS = %s; CME velocity and (front) acceleration %s %s', mdlos, velocity, accel)
    """
    # because velocity is in km/s and mdlos is in a.u.
    # prop_time = (mdlos*km2au)/ velocity
    # prop_time = (-2.*mdlos*km2au)/(velocity + np.sqrt(velocity**2. - 2*accel*mdlos*km2au))#
    """
    prop_time_0 = (-2. * mdlos * km2au) / (velocity + np.sqrt(velocity ** 2. - 2 * accel * mdlos * km2au))
    prop_time_1 = (-2. * mdlos * km2au) / (velocity + np.sqrt(velocity ** 2. + 2 * accel * mdlos * km2au))
    if (prop_time_0 > 0) or (prop_time_1 > 0):
        prop_time = max(prop_time_0, prop_time_1)
    else:
        prop_time = (mdlos * km2au) / velocity

    return prop_time


def final_radius(width=90., velocity=450., mdlos=1.0, accel=0.):
    """

    Parameters
    ----------
    width
    velocity
    mdlos
    accel

    Returns
    -------

    """
    KM2AU = 1e7  # TODO - cleanup, check value
    expansion_velocity_final = expansion_velocity_gdya09(width, velocity)
    prop_time = propagation_time(expansion_velocity_final, mdlos, accel)
    final_radius = velocity * 1e7 * prop_time + 0.5 * accel * (prop_time ** 2.)
    return final_radius


def estimate_dmcme_spherical_expansion(mass=1e15, angle=90, mdlos=1.0):
    """

    Parameters
    ----------
    mass
    angle
    mdlos

    Returns
    -------
    The dispersion measure

    """
    netot = np.multiply(mass, 1e-3) / _MP  # number of electrons in CME == number of protons
    R = np.linspace(0.001, mdlos, 50)  # in AU
    # simple spherical model for electron density cm^-3
    # np.divide and np.multiply do elementwise operation
    nesw = np.divide(5, np.power(R, 2.))
    Rcme = np.multiply(R, 0.5 * np.deg2rad(angle))
    vol = (_VOLCOEFF * (Rcme ** 3.)) * (_CM2AU ** 3)
    necme = np.divide(netot, vol)
    DMcme = np.multiply(necme, Rcme * 2 * _PC2AU)
    DMsw = np.multiply(nesw, R * _PC2AU)
    return DMcme, DMsw


def estimate_dmcme_gdya09_expansion(mass=1e15, mdlos=1.0,
                                    width=90, velocity=150., accel=0.):
    """

    Parameters
    ----------
    mass
    mdlos
    width
    velocity
    accel

    Returns
    -------

    """
    netot = mass / (_MP * 1e3)  # number of electrons in CME == number of protons
    R = np.linspace(0.001, mdlos, 50)  # in AU
    nesw = np.divide(5, np.power(R, 2.))
    radius_mdlos = final_radius(width, velocity, mdlos, accel) * 1e5
    radius = np.linspace(0.001, radius_mdlos, 5)
    # logging.info(radius)
    vol = (_VOLCOEFF * (radius ** 3.))
    necme = np.divide(netot, vol)
    DMcme = np.multiply(necme, (radius * 2 / _AU2PC))
    DMsw = np.multiply(nesw, (R / _AU2PC))
    return DMcme, DMsw


def plot_dmcme(R, DMcme, DMsw):
    """

    Parameters
    ----------
    R
    DMcme
    DMsw
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.loglog(R, DMcme)
    ax.loglog(R, DMsw, c='r')
    ax.loglog(R, 1e-5 * np.ones(len(R)), c='g')
    ax.set_ylabel('DM_{CME}')
    ax.set_xlabel('Solar Distance in AU')
    fig.savefig('DMplot_{}.png'.format(R[-1]))


# File helpers
def download_file(filename, path='/data/ancillary/attitude/predictive/', url='https://sohowww.nascom.nasa.gov'):
    """

    Parameters
    ----------
    filename
    path
    url

    Returns
    -------

    """
    with open(filename, "wb") as fp:
        try:
            curl = pycurl.Curl()
            curl.setopt(pycurl.URL, url + path + filename)
            curl.setopt(pycurl.WRITEDATA, fp)
            curl.perform()
            result = curl.getinfo(pycurl.HTTP_CODE)
            logging.info('File downloaded.')
        except Exception as e:
            logging.error('Cannot download file %s from %s', filename, url)
            logging.exception(e)
            raise SystemExit(1)
        finally:
            curl.close()
        if result == 404:
            os.remove(filename)
            raise pycurl.error('{} not found'.format(filename))
        if result == 301:
            os.remove(filename)
            raise pycurl.error('{} moved to different url'.format(filename))
        return result


def stepinandoutofdir(dirname):
    """

    Parameters
    ----------
    dirname

    Returns
    -------

    """

    def decorator(func):
        def wrapper(*args, **kwargs):
            owd = os.getcwd()
            os.makedirs(dirname, exist_ok=True)
            os.chdir(dirname)
            try:
                func(*args, **kwargs)
            finally:
                os.chdir(owd)

        return wrapper

    return decorator


# LASCO overlay
def lasco_source_textblock(ax, source_name, obstimeiso, pa_psr_ang_float, mdlos):
    textdata(ax, "Obs. Date : {}".format(obstimeiso), -0.08, 1.05, mdlos, halign='left')
    textdata(ax, "Source : {}".format(source_name), -0.08, 1.00, mdlos, halign='left')
    textdata(ax, "Source Helio PA : {}".format(pa_psr_ang_float), -0.08, 0.95, mdlos, halign='left')


def lasco_cme_textblock(ax, cmetimeiso, cmepa, mdlos):
    textdata(ax, "CME launch Date : {}".format(cmetimeiso), 1.09, -0.07, mdlos, halign='right')
    textdata(ax, "CME launch Helio PA : {}".format(cmepa), 1.09, -0.02, mdlos, halign='right')
    return 0


def lasco_title_textblock(ax, titletext, mdlos):
    textdata(ax, titletext, 1.09, 1.05, mdlos, halign='right')
    return 0


def lasco_derotator(dayofyear):
    offset = np.empty(12)
    offset[0] = -7.23 * np.sin((2. * (265. / 365.) * (dayofyear + 10.)))
    offset[1] = -7.23 * np.cos((2. * (265. / 365.) * (dayofyear + 10.)))
    offset[2] = -7.23 * np.tan((2. * (265. / 365.) * (dayofyear + 10.)))
    offset[3] = -7.23 * np.sin(((265. / 365.) * (dayofyear + 10.)))
    offset[4] = -7.23 * np.cos(((265. / 365.) * (dayofyear + 10.)))
    offset[5] = -7.23 * np.tan(((265. / 365.) * (dayofyear + 10.)))
    offset[6] = -7.23 * np.sin((2. * (79. / 365.) * (dayofyear + 10.)))
    offset[7] = -7.23 * np.cos((2. * (79. / 365.) * (dayofyear + 10.)))
    offset[8] = -7.23 * np.tan((2. * (79. / 365.) * (dayofyear + 10.)))
    offset[9] = -7.23 * np.sin(((79. / 365.) * (dayofyear + 10.)))
    offset[10] = -7.23 * np.cos(((79. / 365.) * (dayofyear + 10.)))
    offset[11] = -7.23 * np.tan(((79. / 365.) * (dayofyear + 10.)))
    return offset


def test_lasco_derotation(ax, obsdate, pa_psr_ang, mdlos, alpha):
    offset_c = lasco_derotator(float(Time(obsdate, format='mjd').yday.split(':')[1]))
    ax.plot(np.deg2rad(pa_psr_ang) - offset_c[1] * u.deg, mdlos, marker='+', markersize=40, mew=0.5,
            alpha=alpha,
            markerfacecolor=None, markeredgecolor='blue', linewidth=0.5, zorder=9)

    ax.plot(np.deg2rad(pa_psr_ang) - offset_c[7] * u.deg, mdlos, marker='x', markersize=40, linestyle='dashed',
            mew=0.5, alpha=alpha,
            markerfacecolor=None, markeredgecolor='blue', linewidth=0.5, zorder=9)

    return 0


def add_polar_axis(figpolar, polar_bounds, source_name, obsdate,
                   pa_psr_ang, mdlos, cmedate, cmepa, color, edgecolor, alpha):
    with plt.rc_context(rc={'font.size': 18}):
        plt.gca()
        ax_c3polar = figpolar.add_axes(polar_bounds, projection="polar", facecolor=None)
        ax_c3polar.set_theta_offset(np.pi / 2.)
        ax_c3polar.patch.set_alpha(0)
        ax_c3polar.set_alpha(0.1)
        ax_c3polar.set_ylim(0, 1.23 * mdlos)
        ax_c3polar.plot(np.deg2rad(pa_psr_ang), mdlos, marker='o', markersize=15, mew=1.0, alpha=alpha,
                        markerfacecolor=color, markeredgecolor=edgecolor, linewidth=1.0, zorder=9)
        ax_c3polar.plot(np.deg2rad(pa_psr_ang), mdlos, marker='+', markersize=40, mew=0.5, alpha=alpha,
                        markerfacecolor=None, markeredgecolor=edgecolor, linewidth=0.5, zorder=9)
        ax_c3polar.plot(np.linspace(0, 2 * np.pi, 100), np.ones(100) * 0.14, color='darkgray', linestyle='--',
                        linewidth=1.5)

        axiscolors(ax_c3polar, mdlos)

        try:
            if cmepa.lower().find('halo') != -1:
                cmepa = 360.
        except Exception as e:
            logging.warning('cmepa is likely column of floats')
            pass

        cmepa = cmepa * u.deg

        _obstimeiso = Time(obsdate, format='mjd', out_subfmt='date_hm').iso
        _pa_psr_ang_float = float(int(pa_psr_ang.value))
        _cmetimeiso = str(Time(convert_dt64_to_Time(cmedate).iso, format='iso', out_subfmt='date_hm').iso)

        _titulartext = "LASCO+EIT Composite at CME launch time. \n Source position at observation date."

        ax_c3polar.grid(True, color='lightgrey', linestyle='dotted', zorder=5)

        lasco_source_textblock(ax_c3polar, source_name, _obstimeiso, _pa_psr_ang_float, mdlos)
        lasco_cme_textblock(ax_c3polar, _cmetimeiso, cmepa, mdlos)
        lasco_title_textblock(ax_c3polar, _titulartext, mdlos)

    return ax_c3polar


def add_lascomap_axis(ax_lasco, psr_a_hpc, _edgecolor, _alpha):
    ax_lasco.patch.set_alpha(.01)
    lon, lat = ax_lasco.coords
    lon.set_major_formatter('d.dd')
    lat.set_major_formatter('d.dd')
    lon.set_ticks_visible(False)
    lat.set_ticks_visible(False)
    lat.set_ticklabel_visible(False)
    lon.set_ticklabel_visible(False)
    ax_lasco.plot_coord(psr_a_hpc, marker='s', color='gold', alpha=1 - _alpha,
                        fillstyle='none', markersize=12, mew=0.5, markerfacecolor='gold',
                        markeredgecolor=_edgecolor, linewidth=0.5)

    # ax_lasco = rotate_axis(ax_lasco, rotation=180.)


def sunpymethod(file_jp2, psr_a, image_bounds, _edgecolor, _alpha,
                sourcename, cmedate, pa_psr_ang, mdlos):
    plt.gcf().clear()
    sunpyfig = plt.gcf()
    sunpyfig.set_size_inches(12, 12)
    new_lasco_map = sunpy_map.Map(file_jp2)
    soho = get_horizons_coord('SOHO', new_lasco_map.date)
    new_lasco_map.meta['HGLN_OBS'] = soho.lon.to('deg').value
    new_lasco_map.meta['HGLT_OBS'] = soho.lat.to('deg').value
    new_lasco_map.meta['DSUN_OBS'] = soho.radius.to('m').value
    psr_a_hpc = psr_a.transform_to(new_lasco_map.coordinate_frame)
    # new_lasco_map.meta['CROTA'] += 180.
    # ax_newlascomap = sunpyfig.add_axes([0.13,0.11,0.77,0.77], projection=new_lasco_map)
    ax_newlascomap = plt.subplot(projection=new_lasco_map)
    # ax_newlascomap.patch.set_alpha(.01)
    lon, lat = ax_newlascomap.coords
    lon.set_major_formatter('d.dd')
    lat.set_major_formatter('d.dd')
    lon.set_ticks_visible(False)
    lat.set_ticks_visible(False)
    lat.set_ticklabel_visible(False)
    lon.set_ticklabel_visible(False)
    ax_newlascomap.plot_coord(psr_a_hpc, marker='s', alpha=_alpha, fillstyle='none',
                              markersize=12, mew=0.5, markerfacecolor='gold',
                              markeredgecolor=_edgecolor, linewidth=0.5, zorder=3)
    new_lasco_map.plot(alpha=01.0)
    sunpyfig.savefig('{}_C3_{}_{:.0f}_{:.4f}_sunpymap.png'.format(sourcename, cmedate,
                                                                  pa_psr_ang, mdlos))
    plt.close(sunpyfig)


def add_image_axis(image_axis, img):
    image_axis.imshow(img * 1.1, alpha=0.75, origin='upper')
    image_axis.set_yticklabels([])
    image_axis.set_xticklabels([])
    return 0


def test_sunpymap_overlay(args, psr_a_hpc, fig_LASCO,
                          image_x0, image_y0, image_width, image_height,
                          lasco_map, _edgecolor, _alpha):
    if args.psrname == "Regulus":
        axes_lmap = plt.Axes(fig_LASCO, [image_x0 + 0.0055, image_y0 - 0.003,
                                         image_width - 0.02, image_height - 0.02])
    else:
        axes_lmap = plt.Axes(fig_LASCO, [image_x0 + 0.0055, image_y0 - 0.003,
                                         image_width - 0.024, image_height - 0.024])
    # Disable the axis and add them to the figure.
    axes_lmap.set_axis_off()
    fig_LASCO.add_axes(axes_lmap)
    origin = 0.5
    exval = 1024
    lasco_map.plot(annotate=False, alpha=0.01)
    ax_lasco = fig_LASCO.add_axes([image_x0, image_y0, image_width, image_height], projection=lasco_map)

    lasco_map.plot(axes=axes_lmap, annotate=False, title=None,
                   cmap='cubehelix', alpha=_alpha,
                   extent=(origin, exval, origin, exval))
    add_lascomap_axis(ax_lasco, psr_a_hpc, _edgecolor, _alpha)
    return 0


def lasco_overlay(source_name, cmedate, pa_psr_ang, mdlos, cmepa, obsdate, star_list, psr_a, color, args):
    """

    Parameters
    ----------
    cmedate
    pa_psr_ang
    mdlos
    cmepa
    obsdate
    star_list

    Returns
    -------
    LASCO image overlaid with the heliocentric plot of the source

    """
    # The LASCO plot requires a rescaling of the polar axis. MDLOS is rescaled to match the polar axis.
    mdlos = mdlos * 0.96
    _edgecolor = color
    _alpha = 0.25
    _color = color
    if mdlos < 0.14:
        _edgecolor = 'white'
        _alpha = 0.9
        _color = "None"

    hv = HelioviewerClient(url='https://helioviewer-api.ias.u-psud.fr')
    logging.info("On %s, CME launched from %s. The source is at PA %s & MDLOS is %s on Obsv date %s.",
                 cmedate, cmepa, pa_psr_ang, mdlos, obsdate)
    file = hv.download_png('{}'.format(cmedate), 56,
                           "[SOHO,LASCO,C3,white-light,1,100],[SOHO,LASCO,C2,white-light,1,100],[SDO,AIA,AIA,304,1,100]",
                           x0=0, y0=0, width=1024, height=1024)
    file_jp2 = hv.download_jp2('{}'.format(Time(obsdate, format='mjd', out_subfmt='date_hm').isot),
                               observatory='SOHO', instrument='LASCO', detector='C3')
    lasco_map = sunpy_map.Map(file_jp2)
    psr_a_wrong = psr_a.transform_to(lasco_map.coordinate_frame)
    soho = get_horizons_coord('SOHO', lasco_map.date)
    lasco_map.meta['HGLN_OBS'] = soho.lon.to('deg').value
    lasco_map.meta['HGLT_OBS'] = soho.lat.to('deg').value
    lasco_map.meta['DSUN_OBS'] = soho.radius.to('m').value
    psr_a_hpc = psr_a.transform_to(lasco_map.coordinate_frame)

    crota = lasco_map.meta['CROTA'] * u.deg
    lasco_map.meta['CROTA'] += 180.

    img = imread(file)
    try:
        # Solar radius in AU
        polar_x0 = 0.1
        polar_y0 = 0.1
        polar_width = 0.8
        polar_height = 0.8
        axes_bounds = [polar_x0, polar_y0, polar_width, polar_height]

        image_width = (32. * _RSOL_AU * polar_height) / (1.3333 * mdlos)
        image_height = (32. * _RSOL_AU * polar_height) / (1.3333 * mdlos)
        image_x0 = (1. - image_width) / 2.
        image_y0 = (1. - image_height) / 2.

        # Start  image
        import matplotlib.transforms as mtransforms
        from math import trunc
        plt.gcf().clear()
        fig_LASCO = plt.gcf()
        fig_LASCO.set_size_inches(12, 12)
        ax_image = fig_LASCO.add_axes([image_x0, image_y0, image_width, image_height])

        r = np.sqrt((psr_a_hpc.Tx - psr_a_wrong.Tx) ** 2 +
                    (psr_a_hpc.Ty - psr_a_wrong.Ty) ** 2)
        logging.info('Estimated error in position is %s', r)

        # test_sunpymap_overlay(args, psr_a_hpc, fig_LASCO, image_x0, image_y0, image_width, image_height,
        #                      lasco_map, _edgecolor, _alpha)

        add_image_axis(ax_image, img)

        ax_polar = add_polar_axis(fig_LASCO, axes_bounds, args.psrname,
                                  obsdate, pa_psr_ang, mdlos, cmedate, cmepa, _color, _edgecolor, _alpha)

        fig_LASCO.savefig(
            '{}_C3_{}_{:.0f}_{:.4f}_originrotated.png'.format(args.psrname, cmedate, pa_psr_ang,
                                                              mdlos))
        plt.close(fig_LASCO)

        if args.do_sunpytest:
            sunpy_image_bounds = [image_x0, image_y0, image_width, image_height]
            sunpymethod(file_jp2, psr_a, sunpy_image_bounds, _edgecolor, _alpha, args.psrname, cmedate, pa_psr_ang,
                        mdlos)
        return 0

    except Exception as e:
        logging.warning(e)
        logging.info('LASCO overlay failed!')
        pass


def get_colors(min_val, max_val, date, array_length, nsteps=12):
    """

    Parameters
    ----------
    min_val
    max_val
    date
    array_length
    nsteps

    Returns
    -------

    """
    return plt.cm.viridis(plt.Normalize(min_val, max_val)(date),
                          array_length / nsteps)


def make_lasco_plot(AllCMEs_daily, pa_psr_ang, mdlos, obsdate, color, psr_a, args, star_list=zip([], [])):
    """

    Parameters
    ----------
    AllCMEs_daily
    pa_psr_ang
    mdlos
    obsdate
    color
    psr_a
    args
    star_list

    Returns
    -------

    """
    for cmedate, cmepa in zip(AllCMEs_daily.Date_Time.values, AllCMEs_daily.CentralPA.values):
        if not (cmepa is None):
            star_list = zip([], [])
            try:
                # lasco_overlay(args.source_name, cmedate, pa_psr_ang, mdlos, cmepa, obsdate, star_list, psr_a, color, args)
                print(cmedate)
                print('----------------------------------------------------------------')
                print(pa_psr_ang, mdlos, cmepa, obsdate, star_list, psr_a, color, args)
                lasco_overlay(cmedate, pa_psr_ang, mdlos, cmepa, obsdate, star_list, psr_a, color, args)

            except Exception as e:
                logging.warning(e)
                logging.warning('lasco_overlay plot failed. Not trying wc3_overlay')


def setup_colors(mdlos, color):
    _edgecolor = 'white'
    _alpha = 0.9
    if mdlos < 0.14:
        _edgecolor = color
        _alpha = 0.25
    return _alpha, _edgecolor


def lasco_map(obstime):
    """

    Parameters
    ----------
    obstime

    Returns
    -------

    """
    get_soho_location(obstime)
    return 0


def get_soho_location(obstime):
    """

    Parameters
    ----------
    obstime

    Returns
    -------

    """
    from astropy.coordinates import SkyCoord
    from sunpy.coordinates import frames
    from sunpy.coordinates.ephemeris import get_horizons_coord
    soho = get_horizons_coord('SOHO', obstime.iso)
    # lasco.meta['HGLN_OBS'] = soho.lon.to('deg').value
    # asco.meta['HGLT_OBS'] = soho.lat.to('deg').value
    # lasco.meta['DSUN_OBS'] = soho.radius.to('m').value
    data = np.random.rand(1024, 1024)
    my_coord = SkyCoord(soho.lon.to('deg').value, soho.lat.to('deg').value, obstime=obstime, observer='soho',
                        frame=frames.Helioprojective)
    # sunpy_map.make_fitswcs_header(data, coordinate, reference_pixel: Unit("pix") = None, scale: Unit("arcsec / pix") = None, \
    #    rotation_angle: Unit("deg") = None, rotation_matrix=None, instrument=None, telescope=None, observatory=None, \
    #    wavelength: Unit("Angstrom") = None, exposure: Unit("s") = None, projection_code='TAN')
    my_header = sunpy_map.make_fitswcs_header(data, my_coord)
    my_map = sunpy_map.Map(data, my_header)


# Cactus database readers
@stepinandoutofdir
def fetch_cactus_secchi_database_file():
    """

    Returns
    -------

    """
    download_file(filename='secchi_cmecat_combo.sav', path='/cactus', url='http://secchi.nrl.navy.mil')


def cactus_secchi_reader(secchi_path):
    """

    Parameters
    ----------
    secchi_path

    Returns
    -------

    """
    secchi_default = "./CACTUS_CATALOUGE/secchi_cmecat_combo.sav"
    if (not os.path.exists(secchi_default)) or (not os.path.exists(secchi_path)):
        fetch_cactus_secchi_database_file()
    data = readsav('./CACTUS_CATALOUGE/secchi_cmecat_combo.sav')
    secchia = pd.DataFrame(data['secchia_combo'])
    secchib = pd.DataFrame(data['secchib_combo'])
    return secchia, secchib


def get_cactus_cmesindate_properties(times, newt, mdlos, obsdate):
    """

    Parameters
    ----------
    times
    newt
    mdlos
    obsdate

    Returns
    -------

    """
    cmes_in_date = pd.DataFrame(
        times.iloc[np.where(np.logical_and((newt >= (obsdate - 3.0)), (newt <= obsdate)))])

    # JULTIME, ENDJULTIME, MIN_SPEED, MAX_SPEED
    cmes_in_date['CentralPA'] = cmes_in_date['ANGLE'].apply(pd.to_numeric, errors='coerce')
    cmes_in_date['Width'] = cmes_in_date['WIDTH'].apply(pd.to_numeric, errors='coerce')
    cmes_in_date['LinearSpeed'] = cmes_in_date['SPEED'].apply(pd.to_numeric, errors='coerce')

    cmes_in_date['Accel'] = (cmes_in_date['MAX_SPEED'].apply(pd.to_numeric, errors='coerce') - cmes_in_date[
        'MIN_SPEED'].apply(pd.to_numeric, errors='coerce')) / ((cmes_in_date['JULTIME'].apply(pd.to_numeric,
                                                                                              errors='coerce') -
                                                                cmes_in_date['ENDJULTIME'].apply(pd.to_numeric,
                                                                                                 errors='coerce')) * 24 * 60 * 60)
    cmes_in_date['mdlos'] = mdlos
    cmes_in_date['Propagation_time_in_s'] = [propagation_time(velocity, mdlos, accel) for velocity, mdlos, accel in
                                             zip(cmes_in_date.LinearSpeed, cmes_in_date.mdlos, cmes_in_date.Accel)]
    cmes_in_date['ArrivalTime'] = Time(
        cmes_in_date.Date_Time + cmes_in_date.Propagation_time_in_s.astype("timedelta64[s]"), format='datetime64')
    logging.info('\x1b[0;33;40m' + 'Succeeded on {:.1f}!'.format(obsdate) + '\x1b[0m')
    cmes_in_date = cmes_in_date[cmes_in_date.ArrivalTime <= Time(obsdate, format='mjd')]
    CenPA = Angle(cmes_in_date.CentralPA, u.deg)
    Width = Angle(cmes_in_date.Width, u.deg)
    Mass = cmes_in_date.Mass
    clockwise_limit = np.floor((CenPA - Width / 2.))
    anticlock_limit = np.ceil(CenPA + Width / 2.)
    return cmes_in_date, anticlock_limit, clockwise_limit


def cactus_lasco_reader(LASCO_path):
    """

    Parameters
    ----------
    LASCO_path

    Returns
    -------

    """
    LASCO_default = "./CACTUS_CATALOUGE/cmecat_combo.sav"
    if (not os.path.exists(LASCO_default)) or (not os.path.exists(LASCO_path)):
        fetch_cactus_lasco_database_file()
    data = readsav('/home/n1325/Downloads/cmecat_combo.sav')
    LASCOa = pd.DataFrame(data['LASCO'])
    LASCOb = pd.DataFrame(data['LASCO'])
    return LASCOa, LASCOb


def fetch_cactus_lasco_database_file():
    """

    Returns
    -------

    """
    owd = os.getcwd()
    if not os.path.exists("CACTUS_CATALOUGE"):
        os.makedirs("CACTUS_CATALOUGE")
    os.chdir("./CACTUS_CATALOUGE")
    download_file(filename='cmecat_combo.sav', path='/cactus/catalog/LASCO/2_5_0/', url='http://sidc.oma.be')
    os.chdir(owd)


def search_cactus_lasco(times, newt, mdlos, obsdate):
    """

    Parameters
    ----------
    times
    newt
    mdlos
    obsdate

    Returns
    -------

    """
    cmes_in_date = pd.DataFrame(
        times.iloc[np.where(np.logical_and((newt >= (obsdate - 3.0)), (newt <= obsdate)))])

    # JULTIME, ENDJULTIME, MIN_SPEED, MAX_SPEED
    cmes_in_date['CentralPA'] = cmes_in_date['ANGLE'].apply(pd.to_numeric, errors='coerce')
    cmes_in_date['Width'] = cmes_in_date['WIDTH'].apply(pd.to_numeric, errors='coerce')
    cmes_in_date['LinearSpeed'] = cmes_in_date['SPEED'].apply(pd.to_numeric, errors='coerce')

    cmes_in_date['Accel'] = (cmes_in_date['MAX_SPEED'].apply(pd.to_numeric, errors='coerce') - cmes_in_date[
        'MIN_SPEED'].apply(pd.to_numeric, errors='coerce')) / ((cmes_in_date['JULTIME'].apply(pd.to_numeric,
                                                                                              errors='coerce') -
                                                                cmes_in_date['ENDJULTIME'].apply(pd.to_numeric,
                                                                                                 errors='coerce')) * 24 * 60 * 60)
    cmes_in_date['mdlos'] = mdlos
    cmes_in_date['Propagation_time_in_s'] = [propagation_time(velocity, mdlos, accel) for velocity, mdlos, accel in
                                             zip(cmes_in_date.LinearSpeed, cmes_in_date.mdlos, cmes_in_date.Accel)]
    cmes_in_date['ArrivalTime'] = Time(
        cmes_in_date.Date_Time + cmes_in_date.Propagation_time_in_s.astype("timedelta64[s]"), format='datetime64')
    logging.info('\x1b[0;33;40m' + 'Succeeded on {:.1f}!'.format(obsdate) + '\x1b[0m')
    cmes_in_date = cmes_in_date[cmes_in_date.ArrivalTime <= Time(obsdate, format='mjd')]
    CenPA = Angle(cmes_in_date.CentralPA, u.deg)
    Width = Angle(cmes_in_date.Width, u.deg)
    Mass = cmes_in_date.Mass
    clockwise_limit = np.floor((CenPA - Width / 2.))
    anticlock_limit = np.ceil(CenPA + Width / 2.)
    return cmes_in_date, anticlock_limit, clockwise_limit


def fetchread_cactus_catalogs(cmecatalog):
    """
    Create the dataframe containing the cactus secchi and lasco catalogs

    Returns
    -------

    """
    if (cmecatalog == None) or not (os.path.exists(cmecatalog)):
        fetch_cactus_lasco_database_file(cmecatalog)
        fetch_cactus_secchi_database_file(cmecatalog)
        times = cactus_lasco_reader('')
        times = cactus_secchi_reader('')

    _t = np.array(times.Date_Time.values.tolist())
    newt = Time(_t.view('i8') * u.nanosecond, format='unix', precision=9).mjd
    return times, newt


# CDAW database readers
@stepinandoutofdir("CDAW_CATALOUGE")
def fetch_cdaw_database_file(cdaw_path="./CDAW_CATALOUGE/univ_all.txt"):
    """

    Parameters
    ----------
    cdaw_path

    Returns
    -------

    """
    if not os.path.exists(cdaw_path):
        logging.warning('CDAW CME Catalog not found! Downloading now...')
        download_file(filename='univ_all.txt', path='/CME_list/UNIVERSAL/text_ver/', url='https://cdaw.gsfc.nasa.gov')


def read_cdaw_cmecatalog(cmecatalog):
    """

    Parameters
    ----------
    cmecatalog

    Returns
    -------

    """
    mynames = ['Date', 'Time', 'CentralPA', 'Width', 'LinearSpeed', '2ndSpeedInitial', '2ndSpeedFinal',
               '2ndSpeed20Rsun', 'Accel', 'Mass', 'KineticEnergy', 'MPA', 'Remarks', 'Extras']
    times = pd.read_csv(cmecatalog,
                        names=mynames,
                        parse_dates=[['Date', 'Time']],
                        sep=r"[ ]{2,}|\t",
                        skiprows=[0, 1, 2, 3],
                        engine='python')
    times = times.replace('\*', '', regex=True)
    times = times.replace('-------', 0.0, regex=True)  # .astype(float)
    halo_cmes = times[(times.CentralPA == 'Halo') | (times.CentralPA == 'halo')]
    times = times.replace('Halo', 360.0, regex=True)
    return times


def get_cdaw_cmesindate_properties(times, newt, obsdate, mdlos):
    """

    Parameters
    ----------
    times
    newt
    mdlos
    obsdate

    Returns
    -------

    """
    try:
        cmes_in_date = pd.DataFrame(
            times.iloc[np.where(np.logical_and((newt >= (obsdate - 3.0)), (newt <= obsdate)))])
        cmes_in_date['CentralPA'] = cmes_in_date['CentralPA'].apply(pd.to_numeric, errors='coerce')
        cmes_in_date['Width'] = cmes_in_date['Width'].apply(pd.to_numeric, errors='coerce')
        cmes_in_date['Mass'] = cmes_in_date['Mass'].apply(pd.to_numeric, errors='coerce')
        cmes_in_date['Accel'] = cmes_in_date['Accel'].apply(pd.to_numeric, errors='coerce')
        cmes_in_date['LinearSpeed'] = cmes_in_date['LinearSpeed'].apply(pd.to_numeric, errors='coerce')
        cmes_in_date['mdlos'] = mdlos
        cmes_in_date['Propagation_time_in_s'] = [propagation_time(velocity, mdlos, accel) for velocity, mdlos, accel in
                                                 zip(cmes_in_date.LinearSpeed, cmes_in_date.mdlos, cmes_in_date.Accel)]
        cmes_in_date['ArrivalTime'] = Time(
            cmes_in_date.Date_Time + cmes_in_date.Propagation_time_in_s.astype("timedelta64[s]"), format='datetime64')
        # logging.info('\x1b[0;33;40m'+ 'Found the following CMEs for observation date {} \n {}'.format(obsdate, cmes_in_date.ArrivalTime) +'\x1b[0m')
        cmes_in_date = cmes_in_date[cmes_in_date.ArrivalTime <= Time(obsdate, format='mjd')]
        CenPA = Angle(cmes_in_date.CentralPA, u.deg)
        Width = Angle(cmes_in_date.Width, u.deg)
        Mass = cmes_in_date.Mass
        clockwise_limit = np.floor((CenPA - Width / 2.))
        anticlock_limit = np.ceil(CenPA + Width / 2.)
        return cmes_in_date, anticlock_limit, clockwise_limit
    except Exception as e:
        logging.info('No CMEs found!')
        logging.info(e)


def make_cdaw_cmelist(AllCMEs, cmes_in_date, pa_psr_ang, anticlock_limit, clockwise_limit, obsdate):
    """

    Parameters
    ----------
    AllCMEs
    cmes_in_date
    pa_psr_ang
    anticlock_limit
    clockwise_limit
    obsdate

    Returns
    -------

    """
    AllCMEs = pd.DataFrame.append(AllCMEs, cmes_in_date[(cmes_in_date.CentralPA == 'halo') | \
                                                        (cmes_in_date.CentralPA == 'Halo') | \
                                                        (cmes_in_date.Width == 360.0)])
    AllCMEs = pd.DataFrame.append(AllCMEs,
                                  cmes_in_date[(pa_psr_ang <= anticlock_limit) &
                                               (pa_psr_ang >= clockwise_limit)])

    t = np.array(AllCMEs.Date_Time.values.tolist())
    newt2 = Time(t.view('i8') * u.nanosecond, format='unix', precision=9).mjd

    AllCMEs_daily = AllCMEs.iloc[
        np.where(np.logical_and((newt2 >= (obsdate - 2.0)), (newt2 <= obsdate)))]
    AllCMEs_daily['CentralPA'] = AllCMEs_daily['CentralPA'].apply(pd.to_numeric, errors='coerce')
    CenPA2 = Angle(AllCMEs_daily.CentralPA, u.deg)
    AllCMEs_daily['Width'] = AllCMEs_daily['Width'].apply(pd.to_numeric, errors='coerce')
    Width2 = Angle(AllCMEs_daily.Width, u.deg)
    AllCMEs_daily['Mass'] = AllCMEs_daily['Mass'].apply(pd.to_numeric, errors='coerce')
    try:
        AllCMEs.drop_duplicates(inplace=True)
    except TypeError as e:
        logging.exception(e)
        pass

    return AllCMEs, AllCMEs_daily, CenPA2, Width2


def save_cdaw_cmelist(AllCMEs, dmcme_df, args, obsdate):
    """

    Parameters
    ----------
    AllCMEs
    dmcme_df
    args
    obsdate

    Returns
    -------

    """
    AllCMEs.to_csv('{}_FinalCMElist.txt'.format(args.source_name), sep='\t', float_format='%g')
    if not (dmcme_df.empty):
        try:
            dmcme_df.drop_duplicates(inplace=True)
            dmcme_means = dmcme_df.groupby('ObsDate').mean()
            dmcme_means.to_csv('{}_summed_dmcme_per_obs.txt'.format(args.source_name), sep='\t',
                               float_format='%g')
            dmcme_df.to_csv('{}_dmcme_per_obs.txt'.format(args.source_name), sep='\t', float_format='%g')
        except ValueError or KeyError:
            pass
    else:
        logging.info('No CME crossings found for MJD {:.0f}.'.format(obsdate))


def fetchread_cdaw_catalog(cmecatalog):
    """
    Create the dataframe containing the cdaw

    Returns
    -------

    """
    if (cmecatalog == None) or not (os.path.exists(cmecatalog)):
        logging.warning('CME catalog not found. Downloading the NASA/CDAW catalog to {}.'.format(cmecatalog))
        fetch_cdaw_database_file(cmecatalog)
        times = read_cdaw_cmecatalog(cmecatalog)
    else:
        logging.warning('Found CME catalog at {}. Using it now.'.format(cmecatalog))
        times = read_cdaw_cmecatalog(cmecatalog)

    _t = np.array(times.Date_Time.values.tolist())
    newt = Time(_t.view('i8') * u.nanosecond, format='unix', precision=9).mjd
    return times, newt


# PSRCAT helpers
def fetchbuild_psrcat(psrcat_path="./psrcat_tar/psrcat", update=True):
    """
    Fetch the psrcat source code and build it if it doesn't exist on the host.

    Returns
    -------
    object
    """
    owd = os.getcwd()
    try:
        if not os.path.exists(psrcat_path) or update:
            download_file(filename='psrcat_pkg.tar.gz', path='/research/pulsar/psrcat/downloads/',
                          url='https://www.atnf.csiro.au')
            os.system('tar -xvf psrcat_pkg.tar.gz')
            os.chdir('./psrcat_tar')
            os.system('/bin/bash makeit')
            logging.info('Build step succeeded for PSRCAT. NOTE - This installation may not show up in your path!')
        elif os.path.exists(psrcat_path):
            print('PSRCAT has been extracted and built in the current directory in a previous run.')

    except Exception as e:
        logging.error('''
                      You requested a coordinate lookup via PSRCAT but no psrcat path was supplied 
                      and I failed to download and/or build PSRCAT in the current directory. Exiting now.
                      Please supply path to psrcat or download and build it manually from :
                      https://www.atnf.csiro.au/research/pulsar/psrcat/downloads/psrcat_pkg.tar.gz
                      ''')
        logging.exception(e)
        raise SystemExit(1)
    os.chdir(owd)
    return './psrcat_tar/psrcat'


def find_psrcat():
    """
    Try to use the which shell function to locate psrcat.
    :return:
    psrcat_status
    """
    psrcat_path = subprocess.run(['which', 'psrcat'], capture_output=True, text=True).stdout
    if not(psrcat_path) or psrcat_path.isspace():
        try:
            psrcat_path = subprocess.run(['command', '-v', 'psrcat'], capture_output=True, text=True, shell=True).stdout

            if psrcat_path.contains('alias'):
                psrcat_path = psrcat_path.split('=').replace("'","")
        except Exception as e:
            logging.warning('Subshell did not allow either command or which to be executed. No PSRCAT_PATH found.')
            psrcat_path = ""
    return psrcat_path

def check_psrcat_exists(psrcat_path, build=False):
    """
    Check if psrcat exists, if not download and build it in the current directory.
    IF it exists, then query psrcat for the source coordinates if they were not supplied on the command line.
    """
    psrcat_call = str(psrcat_path) + ' -v'
    child = subprocess.Popen(psrcat_call, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    child.wait()
    psrcat_status = child.returncode
    return int(psrcat_status)


def populate_from_psrcat(source_name, psrcat_path):
    """
    Populate the source coordinates and velocity from psrcat

    Parameters
    ----------
    source_name : Pulsar for which coordinates are being queried
    psrcat_path : path to find PSRCAT on. If nothing is supplied, PSRCAT is downloaded and
    built in the current working directory.

    Returns
    -------
    gl : Galactic latitude (J2000)
    gb : Galactic longitude (J2000)
    dist :  Distance to pulsar (kpc)
    pml :  Proper motion in latitude
    pmb :  Proper motion in longitude
    pepoch : The epoch at which the position was determined.

    """
    psrcat_path = find_psrcat()
    if psrcat_path == "":
        psrcat_path = './psrcat_tar/psrcat'
        logging.warning('Since I could not find system PSRCAT, testing at {}.'.format(psrcat_path))


    psrcat_status = check_psrcat_exists(psrcat_path)

    if (bool(psrcat_status == 127) ^ bool(psrcat_status == "")):
        try:
            logging.error('Could not find PSRCAT at the standard locations. Trying to download and build psrcat.')
            psrcat_path = fetchbuild_psrcat(psrcat_path='./psrcat_tar/psrcat', update=False)
        except Exception as e:
            logging.error('PSRCAT could not be built. Run psrcat_helpers.py --install or manually download and build psrcat.')
            logging.exception(e)
            raise SystemExit(1)

        logging.info('Found PSRCAT at %s', psrcat_path)
        jname, gl, gb, dist, pml, pmb, pepoch = make_psrcat_call(psrcat_path="./psrcat_tar/psrcat", source_name=source_name)
        return gl, gb, dist, pml, pmb, pepoch
    elif psrcat_status == 1:
        try:
            logging.info('Found PSRCAT at %s', psrcat_path)
            jname, gl, gb, dist, pml, pmb, pepoch = make_psrcat_call(psrcat_path="./psrcat_tar/psrcat",
                                                                     source_name=source_name)
            return gl, gb, dist, pml, pmb, pepoch
        except Exception as e:
            logging.error('PSRCAT could not be built. Run psrcat_helpers.py --install or manually download and build psrcat.')
            logging.exception(e)
            raise SystemExit(1)


def make_psrcat_call(psrcat_path, source_name):
    psrcat_call = [str(psrcat_path), '-nonumber', '-nohead', '-o', 'long_error', '-null', '0.0', '-c', "jname gl gb dist pepoch pml pmb", source_name]
    try:
        psrcat_return = subprocess.run(psrcat_call, text=True, capture_output=True).stdout.replace(" 0.0 ", " 0.0  0.0 ")
        jname, jnref, gl, gb, dist, pepoch, pepochref, pml, pmlerr, pmb, pmberr = psrcat_return.split()
    except ValueError as e:
        logging.error(e)

    logging.info('Pulsar coordinates obtained from psrcat sucessfully!')
    return jname, gl, gb, dist, pml, pmb, pepoch


def delete_quad_elements(cmes, psrs, psrs_quadrant, sun, lascoFOV,
                         _sourcetext1, _sourcetext2, _sourcetext3,
                         _cmetext1, _cmetext2, _titletext1):
    cmes.remove()
    psrs.remove()
    psrs_quadrant.remove()
    sun.remove()
    lascoFOV.remove()
    _sourcetext1.remove()
    _sourcetext2.remove()
    _sourcetext3.remove()
    _cmetext1.remove()
    _cmetext2.remove()
    _titletext1.remove()
    return 0


def quad_source_textblock(ax, source_name, obstimeiso, pa_psr_ang_float):
    _ax1 = textdata_quadplots(ax, "Obs. Date : {}".format(obstimeiso), -0.08, 1.05, halign='left')
    _ax2 = textdata_quadplots(ax, "Source : {}".format(source_name), -0.08, 1.00, halign='left')
    _ax3 = textdata_quadplots(ax, "Source Helio PA : {:0.1f}".format(pa_psr_ang_float), -0.08, 0.95, halign='left')
    return _ax1, _ax2, _ax3


def quad_cme_textblock(ax, cmeseries):
    data = cmeseries.to_frame().reset_index()
    data.columns = ['HelioPA', 'Count']
    formats = {'HelioPA': '{: 10.0f}', 'Count': '{: 6.0f}'}
    formatters = {k: v.format for k, v in formats.items()}
    _stringified_data = data.to_string(formatters=formatters, justify='right', index=False, header=True)
    _ax1 = textdata_quadplots(ax, _stringified_data.replace("360", "Halo"), 0.97, -0.03, halign='center')
    _ax2 = textdata_quadplots(ax, 'CMEs and counts ::', 0.80, -0.07, halign='right')
    return _ax1, _ax2


def quad_title_textblock(ax, titletext):
    return textdata_quadplots(ax, titletext, 1.09, 1.05, halign='right')


def textdata_quadplots(ax, titlestring, xpos, ypos, halign):
    return ax.text(xpos, ypos, titlestring,
                   verticalalignment='center',
                   horizontalalignment=halign,
                   transform=ax.transAxes,
                   color='black', fontsize=24)


def quad_plot(y_max_quad, CenPA2, Width2, mdlos, color, pa_psr_ang, obsdate, psrname, AllCMEs_daily):
    fig2 = plt.figure(figsize=(12, 12))
    axes2 = fig2.add_subplot(111, projection='polar')
    axes2.tick_params(axis='both', which='major', labelsize=18)
    axes2.set_ylim(0, y_max_quad * 1.02)
    axes2.set_theta_offset(np.pi / 2.)
    cmes = axes2.bar(np.deg2rad(CenPA2), np.ones(len(CenPA2)) * mdlos, width=np.deg2rad(Width2),
                     alpha=0.2, color=color)

    sun = axes2.bar(2 * np.pi, _RSOL_AU, width=2 * np.pi, color='gold', alpha=1., zorder=2)
    lascoFOV = axes2.bar(2 * np.pi, _RSOL_AU * 32, width=2 * np.pi, color='blue', alpha=.2)
    psrs = axes2.scatter(np.deg2rad(pa_psr_ang), mdlos, facecolor=color, edgecolor='white', s=3e2,
                         marker='*', zorder=2, alpha=0.5)
    psrs_quadrant = axes2.bar(np.deg2rad(pa_psr_ang), mdlos, width=(np.pi / 2.), color='red',
                              alpha=0.3)
    _sourcetext1, _sourcetext2, _sourcetext3 = quad_source_textblock(axes2, psrname,
                                                                     Time(obsdate, format='mjd', out_subfmt='date').iso,
                                                                     float(pa_psr_ang.value))
    _cmetext1, _cmetext2 = quad_cme_textblock(axes2, AllCMEs_daily.CentralPA.value_counts())
    _titletext1 = quad_title_textblock(axes2, 'All CMEs encountered at MDLOS piercing point.')

    fig2.savefig('{}_Quad_{}_{:.0f}_{:.4f}.png'.format(psrname, Time(obsdate, format='mjd',
                                                                     out_subfmt='date').iso,
                                                       pa_psr_ang, mdlos))
    delete_quad_elements(cmes, psrs, psrs_quadrant, sun, lascoFOV,
                         _sourcetext1, _sourcetext2, _sourcetext3,
                         _cmetext1, _cmetext2, _titletext1)
    plt.close(fig2)
    return 0


# SIMBAD helpers
def get_stars_around_sun(cmedate):
    """
    Check for stars around the sun, if found overplot location markers. NEEDS more work.
    """
    sunpos = get_sun.sky_position(t='{}'.format(cmedate))
    sunpos_sc = SkyCoord(ra=sunpos[0], dec=sunpos[1], frame='icrs')
    separation = 4 * u.deg
    script = ''
    for position_angle in range(0, 360, 45):
        offset_position = sunpos_sc.directional_offset_by(position_angle * u.deg, separation)
        if (position_angle == 0):
            script += '(region(box, ICRS, {} {}, 0.5d 0.5d) |'.format(offset_position.ra.value,
                                                                      offset_position.dec.value)
        elif ((position_angle < 360) and (position_angle > 0)):
            script += ' region(box, ICRS, {} {}, 0.5d 0.5d) |'.format(offset_position.ra.value,
                                                                      offset_position.dec.value)

    script += 'region(box, ICRS, {} {}, 0.5d 0.5d))'.format(offset_position.ra.value, offset_position.dec.value)

    # result_df = Simbad.query_region(sunpos_sc,radius=4.5*u.deg, epoch='J2000', equinox=2000).to_pandas()
    # Put 10 circe s around sun toavoid slow queries
    result_df = Simbad.query_criteria(script, otype='Star').to_pandas()
    logging.info('\x1b[0;32;41m' + 'Hoi!' + '\x1b[0m')
    # logging.info('\x1b[6;33;47m' + script + '\x1b[0m')
    result_df.drop_duplicates(inplace=True)
    result_df.RV_VALUE.fillna(0, inplace=True)
    result_df.PMRA.fillna(0, inplace=True)
    result_df.PMDEC.fillna(0, inplace=True)
    result_df = result_df[result_df.OTYPE.str.decode("utf-8") == 'Star']
    # logging.info(result_df.keys())
    # logging.info(result_df["V__magtyp"])
    return result_df.sort_values(['FLUX_H']).dropna(subset=['FLUX_H', 'Distance_distance']).tail(3)


def knownobjects(coord):
    """
    Check if some preselected sources are in the field of view of LASCO and overplot those.
    """
    for object in ["Antares", "Alcyone", "Aldebaran", "Regulus"]:
        result = Vizier.query_region(coord.SkyCoord(ra=299.590, dec=35.201,
                                                    unit=(u.deg, u.deg),
                                                    frame='icrs'),
                                     width="30m",
                                     columns=['_RAJ2000', '_DEJ2000', 'B-V', 'Vmag', 'Plx', "*", "+_r"],
                                     catalog=["GSC", "V/50"],
                                     column_filters={"Vmag": ">8"},
                                     keywords=["optical"])
        result = Vizier.query_source("3C 273", radius=0.1 * u.deg, catalog='GSC')

    v = Vizier(columns=['_RAJ2000', '_DEJ2000', 'PMRA', 'PMDEC', 'B-V', 'Vmag', 'Plx', "*", "+_r"],
               column_filters={"Vmag": ">10"}, keywords=["optical"], radius=0.1 * u.deg),
    result = v.query_region("Antares", catalog=["NOMAD", "UCAC"])


def starlord(star_list):
    """

    Parameters
    ----------
    star_list

    Returns
    -------

    """
    coords = []
    names = []
    for name, ra_d, dec_d, pmra, pmdec, distance, distance_units, rv_value in zip(star_list.MAIN_ID, star_list.RA_d,
                                                                                  star_list.DEC_d, star_list.PMRA,
                                                                                  star_list.PMDEC,
                                                                                  star_list.Distance_distance,
                                                                                  star_list.Distance_unit,
                                                                                  star_list.RV_VALUE):
        if (distance_units == 'kpc'):
            distance = distance * 1e3
        coords.append(SkyCoord(ra=ra_d * u.degree, dec=dec_d * u.degree, distance=distance * u.pc,
                               pm_ra_cosdec=pmra * u.mas / u.yr, pm_dec=pmdec * u.mas / u.yr,
                               radial_velocity=rv_value * u.km / u.s, frame='icrs',
                               equinox='J2000'))  # .transform_to('galactic'))
        names.append(name)
    return zip(names, coords)


def simbad_overlay(cmedate, star_coords, ax_polar):
    """
    Parameters
    ----------
    cmedate
    star_coords
    ax_polar

    Returns
    -------

    """
    sunpos = get_sun.sky_position(t='{}'.format(cmedate))
    for star, coords in star_coords:
        SAngle_radians = get_ang_dist(coords.ra, coords.dec, Angle(sunpos[0]).degree * u.deg, sunpos[1])
        mdlos = np.sin(SAngle_radians)
        tformat = 'isot'
        otype = 'Star'
        pa_star_ang, pa_sun_ignore = get_HelioPA(cmedate, coords, tformat, otype)
        ax_polar.plot(np.deg2rad(pa_star_ang), mdlos, marker='o', markersize=15,
                      markerfacecolor='none', markeredgecolor='yellow')
    return 0


# SOHO helpers
@stepinandoutofdir("SOHO_ATTITUDE_DATA")
def fetch_predictive_roll_angle_file(obsdate):
    """

    Parameters
    ----------
    obsdate

    Returns
    -------

    """
    predpath = 'SO_AT_PRE_{}_V01.DAT'.format(Time(obsdate, format='mjd', out_subfmt='date').iso.replace('-', ''))
    year = Time(obsdate, format='mjd', out_subfmt='date').iso.split('-')[0]
    logging.info('Downloading predictive roll angle file : %s/%s', year, predpath)
    try:
        download_file(predpath, path='/data/ancillary/attitude/predictive/',
                      url='https://sohowww.nascom.nasa.gov')
    except pycurl.error:
        os.makedirs(str(year), exist_ok=True)
        predpath = os.path.join(str(year), predpath)
        download_file(predpath, path='/data/ancillary/attitude/predictive/',
                      url='https://sohowww.nascom.nasa.gov')
    return predpath


@stepinandoutofdir("SOHO_ATTITUDE_DATA")
def fetch_nominal_roll_attitude_file():
    """

    """
    download_file(filename='nominal_roll_attitude.dat', path='/data/ancillary/attitude/roll/')


def get_nominal_roll_angle(cmedate):
    """

    Parameters
    ----------
    cmedate

    Returns
    -------

    """
    filepath = os.path.join("SOHO_ATTITUDE_DATA", "nominal_roll_attitude.dat")
    if not os.path.exists(filepath):
        fetch_nominal_roll_attitude_file()
    nominal_rolls = pd.read_csv(filepath,
                                sep='\s+', engine='python',
                                names=['Date', 'Time', 'Roll_Angle'],
                                comment='#')
    nominal_rolls['Datetime'] = pd.to_datetime(nominal_rolls['Date'].apply(str) + ' ' + nominal_rolls['Time'])
    nominal_rolls = nominal_rolls[Time(nominal_rolls.Datetime) <= Time(cmedate, format='mjd')]
    logging.info('nominal difference %s', Angle(nominal_rolls.Roll_Angle.sum(), unit=u.deg).wrap_at(360 * u.deg).degree)
    nominal_roll_angle = Angle(nominal_rolls.Roll_Angle.sum(), unit=u.deg).wrap_at(360 * u.deg).degree
    return float(nominal_rolls.Roll_Angle.tail(1).values)


def get_predictive_roll_angle(cmedate):
    """

    Parameters
    ----------
    cmedate

    Returns
    -------

    """
    filename = fetch_predictive_roll_angle_file(cmedate)
    predictive_rolls = pd.read_csv('./SOHO_ATTITUDE_DATA/{}'.format(filename),
                                   sep='\s+', engine='python',
                                   names=['Date', 'Time', 'year', 'dayofyear', 'msfrom0',
                                          'SC_avg_pitch_rad', 'SC_avg_roll_rad', 'SC_avg_yaw_rad',
                                          'SC_bdy_pitch_rad', 'SC_bdy_roll_rad', 'SC_bdy_yaw_rad',
                                          'GCI_avg_pitch', 'GCI_avg_roll', 'GCI_avg_yaw',
                                          'GSE_avg_pitch', 'GSE_avg_roll', 'GSE_avg_yaw',
                                          'GSM_avg_pitch', 'GSM_avg_roll', 'GSM_avg_yaw',
                                          'SC_bdy_stddev_pitch', 'SC_bdy_stddev_roll', 'SC_bdy_stddev_yaw',
                                          'SC_bdy_min_pitch', 'SC_bdy_min_roll', 'SC_bdy_min_yaw',
                                          'SC_bdy_max_pitch', 'SC_bdy_max_roll', 'SC_bdy_max_yaw'],
                                   comment='#')
    predictive_rolls['Datetime'] = pd.to_datetime(predictive_rolls['Date'].apply(str) + ' ' + predictive_rolls['Time'])
    predictive_rolls = predictive_rolls[Time(predictive_rolls.Datetime) <= Time(cmedate, format='mjd')]
    logging.info(np.rad2deg(
        predictive_rolls.SC_avg_roll_rad.tail(1).values[0] - predictive_rolls.SC_bdy_roll_rad.tail(1).values[0]))
    return np.rad2deg(
        predictive_rolls.SC_avg_roll_rad.tail(1).values[0] - predictive_rolls.SC_bdy_roll_rad.tail(1).values[0])


def get_soho_roll_angle(cmedate):
    """

    Parameters
    ----------
    cmedate

    Returns
    -------

    """
    try:
        rangle_pred = get_predictive_roll_angle(cmedate)
    except:
        rangle_pred = 0
        logging.info('SOHO predictive roll angle not found!')
        pass
    rangle_nom = get_nominal_roll_angle(cmedate)
    rangle = rangle_nom + rangle_pred
    return rangle


def nominal_roll_correction(time, pa_psr):
    _nominal_roll = get_nominal_roll_angle(time)
    logging.info("_nominal_roll %s", _nominal_roll)
    if _nominal_roll == 0.0:
        pa_psr += 180.0 * u.deg
    return pa_psr


# Generic utils
def get_ang_dist(RA, DEC, RAsun, DECsun):
    """
    Get angular distance to the sun, given RA and DEC of sun and pulsar.

    Parameters
    ----------
    RA
    DEC
    RAsun
    DECsun

    Returns
    -------

    """
    delt_lon = (RA - RAsun) * np.pi / 180.
    delt_lat = (DEC - DECsun) * np.pi / 180.
    # Haversine formula
    dist = 2.0 * np.arcsin(np.sqrt(np.sin(delt_lat / 2.0) ** 2 + \
                                   np.cos(DEC * np.pi / 180.) * np.cos(
        DECsun * np.pi / 180.) * np.sin(delt_lon / 2.0) ** 2))
    return dist / np.pi * 180.


def getAngDist2(ra, dec, obsdate):
    """

    Parameters
    ----------
    ra
    dec
    obsdate

    Returns
    -------

    """
    from astropy.coordinates import FK5
    from astropy.coordinates import get_sun
    from astropy.time import Time
    coo_psr = SkyCoord(ra, dec, frame=FK5)
    t = Time(obsdate, format='mjd')
    sunpos = get_sun(t)
    sep = sunpos.separation(coo_psr)
    return sep.value


def get_DEC(DECsun, rho, phi):
    """
    Get the declination of the sun
    Parameters
    ----------
    DECsun
    rho
    phi

    Returns
    -------

    """
    DEC = np.arcsin(np.sin(DECsun) * np.cos(rho) \
                    + np.cos(DECsun) * np.sin(rho) * np.cos(phi))
    return DEC


def get_RA(RAsun, DECsun, rho, phi):
    """
    Get the RA of the sun
    Parameters
    ----------
    RAsun
    DECsun
    rho
    phi

    Returns
    -------

    """
    RA = np.arctan2((np.sin(rho) * np.sin(phi)) / (np.cos(rho) * np.cos(DECsun)
                                                   - np.sin(rho) * np.sin(DECsun) * np.cos(phi))) + RAsun
    return RA


def sun_inclination_for_dayofyear(dayofyear):
    """
    # Get sun's declination

    Parameters
    ----------
    dayofyear

    Returns
    -------

    """

    sun_inclination = -23.45 * np.cos((360. / 365.) * (dayofyear + 10.))
    return sun_inclination


def get_rho(RA, DEC, RAsun, DECsun, mdlos):
    """

    Parameters
    ----------
    RA
    DEC
    RAsun
    DECsun
    mdlos

    Returns
    -------

    """
    try:
        rho_nom = np.sqrt(np.power(np.cos(DEC) * np.sin(RA - RAsun), 2.) + np.power(
            (np.cos(DECsun) * np.sin(DEC) - np.sin(DECsun) * np.cos(DEC) * np.cos(RA - RAsun)), 2))
        rho_denom = np.sin(DEC) * np.sin(DECsun) + np.cos(DEC) * np.cos(DECsun) * np.cos(RA - RAsun)
        rho = rho_nom / rho_denom
    except:
        rho = np.arccos(np.cos(DEC) * np.cos(DECsun) * np.cos(RA - RAsun) \
                        + np.sin(DEC) * np.sin(DECsun))
    finally:
        logging.info('\n\x1b[6;33;47m' + 'get_rho failed, setting to mdlos' + '\x1b[0m\n')
        rho = mdlos
    return rho


def get_phi(RA, DEC, RAsun, DECsun):
    """

    Parameters
    ----------
    RA
    DEC
    RAsun
    DECsun

    Returns
    -------

    """
    phi = math.atan2(np.sin(RA - RAsun), (np.tan(DEC) * np.cos(DECsun) - np.sin(DECsun) * np.cos(RA - RAsun)))
    return math.degrees(phi)


def heliocentric_position_angle(RA, DEC, RAsun, DECsun):
    """

    Parameters
    ----------
    RA
    DEC
    RAsun
    DECsun

    Returns
    -------

    """
    phi = math.degrees(math.atan((np.sin(RA - RAsun) / (
            np.tan(DEC) * np.cos(DECsun) - np.sin(DECsun) * np.cos(RA - RAsun)))))
    return phi


def get_HelioPA(time_observed, source_coords, tformat='mjd', otype='psr'):
    """

    Parameters
    ----------
    time_observed
    source_coords
    tformat
    otype

    Returns
    -------

    """
    if not (tformat == 'isot'):
        time_observed_isot = Time(time_observed, format=tformat).isot
    else:
        time_observed_isot = time_observed
    # Get sun position in RA-DEC
    sunpos = get_sun.sky_position(t='{}'.format(time_observed_isot))

    # Calculate position angle of pulsar relative to Sun
    try:
        psr_b = source_coords.apply_space_motion(new_obstime=Time(time_observed, format=str(tformat))).transform_to(
            'icrs')

    except:
        psr_b = source_coords

    pa_0 = heliocentric_position_angle(psr_b.ra, psr_b.dec, Angle(sunpos[0]).degree * u.deg, sunpos[1])
    pa_1 = heliocentric_position_angle(Angle(sunpos[0]).degree * u.deg, sunpos[1], Angle(sunpos[0]).degree * u.deg,
                                       sunpos[1])

    # Convert it into an angle in degrees
    pa_psr = Angle(pa_0, u.deg) - get_sun.P(time_observed_isot)
    pa_sun = Angle(pa_1, u.deg)
    # If origin='lower' in ax_image.imshow in LASCO_overlay
    # we need to rotate pa_psr by 180 degrees

    nominal_roll_correction(Time(time_observed_isot, format='isot').mjd, pa_psr)

    # Add CROTA
    pa_psr += get_soho_roll_angle(Time(time_observed_isot, format='isot').mjd) * u.deg

    logging.warning("Sun Inclination is %s",
                    sun_inclination_for_dayofyear(float(Time(time_observed_isot, format='isot').yday.split(':')[1])))

    pa_psr.wrap_at(360 * u.deg, inplace=True)
    return pa_psr, pa_sun


def get_helio_rho(time_observed, source_coords, tformat='mjd', otype='psr'):
    """

    Parameters
    ----------
    time_observed
    source_coords
    tformat
    otype

    Returns
    -------

    """
    if not (tformat == 'isot'):
        time_observed_isot = Time(time_observed, format=tformat).isot
    else:
        time_observed_isot = time_observed
    # Get sun position in RA-DEC
    sunpos = get_sun.sky_position(t='{}'.format(time_observed_isot))

    try:
        psr_b = source_coords.apply_space_motion(new_obstime=Time(time_observed, format=str(tformat))).transform_to(
            'icrs')
    except:
        psr_b = source_coords
    pa_0 = get_phi(psr_b.ra, psr_b.dec, Angle(sunpos[0]).degree * u.deg, Angle(sunpos[1]).degree * u.deg)
    pa_1 = get_phi(Angle(sunpos[0]).degree * u.deg, sunpos[1], Angle(sunpos[0]).degree * u.deg,
                   Angle(sunpos[1]).degree * u.deg)

    pa_psr = Angle(pa_0, u.deg)
    pa_sun = Angle(pa_1, u.deg)
    return pa_psr, pa_sun


def get_mdlos(obsdate, dmvars):
    """

    Parameters
    ----------
    obsdate
    dmvars

    Returns
    -------

    """
    mdlos = np.mean(np.sin(np.deg2rad(dmvars.solarangle[dmvars.dme == obsdate].values)))
    logging.warning("%s is the mdlos", mdlos)
    if np.isnan(mdlos):
        logging.info('\x1b[0;32;41m')
        logging.info(dmvars.solarangle[dmvars.dme.values < np.ceil(obsdate)], np.ceil(obsdate))
        logging.info('\x1b[0m')
        mdlos = np.mean(np.sin(np.deg2rad(dmvars.solarangle[dmvars.dme == obsdate].min())))
    logging.info('\x1b[0;32;41m' + str(mdlos) + str(
        np.mean(np.deg2rad(dmvars.solarangle[dmvars.dme == obsdate].dropna().values))) + '\x1b[0m')
    return mdlos


def rotate_axis(ax, rotation=180.):
    """

    Parameters
    ----------
    ax - Matplotlib axis object to which the rotation is to be applied
    rotation - the angle through the axis is to be rotated

    Returns
    -------
    nothing, rotates axis 'in place'

    """
    r = Affine2D().rotate_deg(rotation)

    for x in ax.images + ax.lines + ax.collections:
        trans = x.get_transform()
        x.set_transform(r + trans)
        if isinstance(x, PathCollection):
            transoff = x.get_offset_transform()
            x._transOffset = r + transoff

    old = ax.axis()
    ax.axis(old[2:4] + old[0:2])
    return ax


def another_rotator(ax, psr_a_hpc):
    """

    Parameters
    ----------
    ax - Matplotlib axis object on which the rotate transformation is to be applied
    psr_a_hpc - Heliographic position angle of the source

    Returns
    -------
    Nothing, axis is rotated 'in place'

    """
    # first of all, the base transformation of the data points is needed
    base = ax.gca().transData
    rot = Affine2D().rotate_deg(90)

    # define transformed line
    line = ax.plot(psr_a_hpc, 'r--', transform=rot + base)
    # or alternatively, use:
    # line.set_transform(rot + base)
    return ax


def switch_axes_colors(ax, color):
    """

    Parameters
    ----------
    ax - Matplotlib axis object ofr the tic label colors are to be chnaged
    color - the color to change the tic label color to

    Returns
    -------
    None

    """
    rlabels = ax.get_ymajorticklabels()
    for label in rlabels:
        label.set_color(color)
    tlabels = ax.get_xmajorticklabels()
    for label in tlabels:
        label.set_color(color)


def textdata(ax, titlestring, xpos, ypos, mdlos, halign):
    """

    Parameters
    ----------
    ax - Matplotlib plot axis on which text data is being inserted.
    titlestring - string that is to be inserted
    xpos - x position in plot units (0,1). Can be greater or less than this values.
    ypos - y position in plot units (0,1). Can be greater or less than this values.
    mdlos - minimum distance to the line of sight
    halign - horizontal alingment keyword for matplotlib.axes.text method

    Returns
    -------
    0 if succeeded

    """
    if mdlos < 0.09:
        ax.text(xpos, ypos, titlestring,
                verticalalignment='center',
                horizontalalignment=halign,
                transform=ax.transAxes,
                color='white', fontsize=24)
    elif (mdlos >= 0.09) and (mdlos < 0.14):
        ax.text(xpos, ypos, titlestring,
                verticalalignment='center',
                horizontalalignment=halign,
                transform=ax.transAxes,
                color='darkgray', fontsize=24)
    elif mdlos >= 0.14:
        ax.text(xpos, ypos, titlestring,
                verticalalignment='center',
                horizontalalignment=halign,
                transform=ax.transAxes,
                color='black', fontsize=24)

    return 0


def axiscolors(ax, mdlos):
    """

    Parameters
    ----------
    ax - axis whose text label colors will be changed
    mdlos - the minimum distance to the line of sight, used to select what the font color should be.

    Returns
    -------

    """
    if mdlos < 0.09:
        switch_axes_colors(ax, 'white')
    elif (mdlos >= 0.09) and (mdlos < 0.14):
        switch_axes_colors(ax, 'darkgray')
    elif mdlos >= 0.14:
        switch_axes_colors(ax, 'black')


def convert_dt64_to_Time(dt64):
    """
    Convert a numpy datetime64 object to an astropy Time object.
    """
    if dt64.dtype.name in ['datetime64[M]', 'datetime64[Y]']:
        dt64 = dt64.astype('M8[D]')

    time_string = str(dt64)
    precision = 3
    if '.' in time_string:
        precision = len(time_string.split('.')[-1])

    return Time(time_string, precision=precision)


# CMEchaser main
def get_times(cmecatalog, catsource):
    if catsource == 'CDAW':
        if cmecatalog == None:
            times, newt = fetchread_cdaw_catalog(cmecatalog="./CDAW_CATALOUGE/univ_all.txt")
        else:
            times, newt = fetchread_cdaw_catalog(cmecatalog)
    elif catsource == 'CACTUS':
        times, newt = fetchread_cactus_catalogs(cmecatalog)
    return times, newt


def setup_logger(logger_name, log_file, level=logging.INFO):
    # set up logging to file - see previous section for more details
    logging.basicConfig(level=level,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename=log_file,
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    logging.info('Starting %s logging', logger_name)
    # Now, define a couple of other loggers which might represent areas in your
    # application:
    logger1 = logging.getLogger('simbad_helpers')
    logger2 = logging.getLogger('translations')
    logger3 = logging.getLogger('quadplotter')
    logger4 = logging.getLogger('psrcat_helpers')
    logger5 = logging.getLogger('lasco_overlays')
    logger6 = logging.getLogger('file_helpers')
    logger7 = logging.getLogger('cdaw')
    logger8 = logging.getLogger('cactus')


def setup_and_parse_program_arguments():
    """

    Returns
    -------

    """
    parser = argparse.ArgumentParser(
        description='Get input parfile and/or timfile, need pulsar name at the least')
    parser.add_argument('-P', '--psrname', help='Name of the pulsar you want to work with',
                        metavar='PSR', default=None, required=True)
    parser.add_argument('--cmecatalog', '-C', help='Path to cmecatalog', metavar='CMECATALOG',
                        default=None)
    parser.add_argument('--dmvarfile', '-D', help='Path to dm variations file', metavar='DMVARFILE',
                        type=str)
    parser.add_argument('--parfile', '-p', help='Path to parfile', metavar='PARFILE', default=None)
    parser.add_argument('--solar_cutoff', '-s', help='Cutoff angle in degrees', metavar='ANGLE',
                        default=55)
    parser.add_argument('--gl', '-l', help='Galactic latitude in degrees', metavar='GL', type=float)
    parser.add_argument('--gb', '-b', help='Galactic longitude in degrees', metavar='GB',
                        type=float)
    parser.add_argument('--pml', help='Galactic latitude proper motion in degrees', metavar='PML',
                        type=float, default=0.0)
    parser.add_argument('--pmb', help='Galactic longitude proper motion in degrees', metavar='PMB',
                        type=float, default=0.0)
    parser.add_argument('--dist', '-d', help='Distance in kiloparsec', metavar='DIST', default=1.0,
                        type=float)
    parser.add_argument('--pepoch', help='Observation epoch MJD', metavar='PEPOCH', type=float)
    parser.add_argument('--radvel', help='Radial velocity', metavar='RADVEL', type=float,
                        default=0.0)
    parser.add_argument('--psrcat', help='Path to psrcat', default="./psrcat_tar/psrcat")
    parser.add_argument('--do_sunpytest', help='Produce the Sunpy Map method overlay', type=bool, default=False)
    parser.add_argument('--catsource', help='Catalogue source. Choose between CDAW and CACTUS', default="CDAW")
    args = parser.parse_args()
    return args


def get_source_coordinates(args):
    """
    Get parameters via the command line

    Returns
    -------
    psr_a

    Parameters
    -------

    """
    if args.gl is None or args.gb is None or args.dist is None:
        _l, _b, _d, _pml, _pmb, _pepoch = populate_from_psrcat(args.psrname, args.psrcat)
        if args.gl is None:
            args.gl = float(_l)
        if args.gb is None:
            args.gb = float(_b)
        if args.dist is None:
            args.dist = float(_d)
        if args.pml is None:
            args.pml = float(_pml)
        if args.pmb is None:
            args.pmb = float(_pmb)
        if args.pepoch is None:
            args.pepoch = float(_pepoch)

    psr_a = SkyCoord(l=args.gl * u.degree, b=args.gb * u.degree, distance=args.dist * u.pc,
                     pm_l_cosb=args.pml * u.mas / u.yr, pm_b=args.pmb * u.mas / u.yr,
                     radial_velocity=args.radvel * u.km / u.s,
                     frame='galactic',
                     obstime=Time(args.pepoch, format='mjd'))

    logging.info('Source Name: %s; Galactic Latitude : %s; Galactic Longitude : %s', args.psrname, args.gl, args.gb)
    return psr_a


def check_create_solarangles(dmvars, psr_a):
    if not ('solarangle' in dmvars.columns):
        dmvars['solarangle'] = 0.
        for obsdate in dmvars.dme:
            time_observed_isot = Time(obsdate, format='mjd').isot
            # Get sun position in RA-DEC
            sunpos = get_sun.sky_position(t='{}'.format(time_observed_isot))
            # Calculate position angle of pulsar relative to Sun
            psr_b = psr_a.apply_space_motion(new_obstime=Time(obsdate, format='mjd')).transform_to('icrs')
            SAngle_radians = get_ang_dist(psr_b.ra, psr_b.dec, Angle(sunpos[0]).degree * u.deg, sunpos[1])
            dmvars.solarangle.loc[(dmvars.dme == obsdate)] = np.rad2deg(SAngle_radians)
    else:
        dmvars['solarangle'] = dmvars['solarangle'].apply(pd.to_numeric, errors='coerce')


def get_dmvars(dateslistfile, solar_cutoff, psr_a):
    try:
        dmvars = pd.read_csv(dateslistfile, sep=r'\t', engine='python')
    except:
        dmvars = pd.read_csv(dateslistfile, sep=r'\s+', engine='python')

    check_create_solarangles(dmvars, psr_a)

    dmvars = dmvars[dmvars.solarangle <= solar_cutoff]
    return dmvars


def get_cmes_in_date(times, newt, obsdate, mdlos):
    cmes_in_date = pd.DataFrame(
        times.iloc[np.where(np.logical_and((newt >= (obsdate - 3.0)), (newt <= obsdate)))])
    cmes_in_date['CentralPA'] = cmes_in_date['CentralPA'].apply(pd.to_numeric, errors='coerce')
    cmes_in_date['Width'] = cmes_in_date['Width'].apply(pd.to_numeric, errors='coerce')
    cmes_in_date['Mass'] = cmes_in_date['Mass'].apply(pd.to_numeric, errors='coerce')
    cmes_in_date['Accel'] = cmes_in_date['Accel'].apply(pd.to_numeric, errors='coerce')
    cmes_in_date['LinearSpeed'] = cmes_in_date['LinearSpeed'].apply(pd.to_numeric, errors='coerce')
    cmes_in_date['mdlos'] = mdlos
    cmes_in_date['Propagation_time_in_s'] = [propagation_time(velocity, mdlos, accel) for velocity, mdlos, accel in
                                             zip(cmes_in_date.LinearSpeed, cmes_in_date.mdlos, cmes_in_date.Accel)]
    cmes_in_date['ArrivalTime'] = Time(
        cmes_in_date.Date_Time + cmes_in_date.Propagation_time_in_s.astype("timedelta64[s]"), format='datetime64')
    cmes_in_date = cmes_in_date[cmes_in_date.ArrivalTime <= Time(obsdate, format='mjd')]
    return cmes_in_date


class CMENotfoundError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


def allcmes(cmes_in_date, pa_psr_ang, obsdate, anticlock_limit, clockwise_limit, mdlos, AllCMEs, dmcme_df):
    AllCMEs_daily = pd.DataFrame()
    index_dmcme_df = ['DMcme_sphX', 'MDLOS', 'ObsDate']

    AllCMEs = pd.DataFrame.append(AllCMEs, cmes_in_date[(cmes_in_date.CentralPA == 'halo') | \
                                                        (cmes_in_date.CentralPA == 'Halo') | \
                                                        (cmes_in_date.Width == 360.0)])
    AllCMEs = pd.DataFrame.append(AllCMEs,
                                  cmes_in_date[(pa_psr_ang <= anticlock_limit) &
                                               (pa_psr_ang >= clockwise_limit)])
    t = np.array(AllCMEs.Date_Time.values.tolist())
    newt2 = Time(t.view('i8') * u.nanosecond, format='unix', precision=9).mjd
    AllCMEs_daily = AllCMEs.iloc[
        np.where(np.logical_and((newt2 >= (obsdate - 2.0)), (newt2 <= obsdate)))]
    AllCMEs_daily['CentralPA'] = AllCMEs_daily['CentralPA'].apply(pd.to_numeric, errors='coerce')
    CenPA2 = Angle(AllCMEs_daily.CentralPA, u.deg)
    AllCMEs_daily['Width'] = AllCMEs_daily['Width'].apply(pd.to_numeric, errors='coerce')
    Width2 = Angle(AllCMEs_daily.Width, u.deg)
    AllCMEs_daily['Mass'] = AllCMEs_daily['Mass'].apply(pd.to_numeric, errors='coerce')

    if not len(AllCMEs_daily.columns) == 0 or AllCMEs_daily.empty:
        for mass, width in zip(AllCMEs_daily.Mass, AllCMEs_daily.Width):
            dmcme, dmsw = estimate_dmcme_spherical_expansion(mass=mass, angle=width, mdlos=mdlos)
            if dmcme[-1] >= 1e-15:
                logging.info('DMcme = {:.2g}; at {:.2g} AU on {:.1f}'.format(dmcme[-1], mdlos, obsdate))
                dmcme_df = pd.DataFrame.append(dmcme_df,
                                               pd.Series([dmcme[-1], mdlos, int(obsdate)], index=index_dmcme_df),
                                               ignore_index=True)
    else:
        logging.info('Spherical DM_cme estimate failed! Setting values to zero.')
        dmcme = np.empty(1)
        dmsw = np.empty(1)
        dmcme_df = pd.DataFrame.append(dmcme_df, pd.Series([dmcme[-1], mdlos, int(obsdate)],
                                                           index=index_dmcme_df), ignore_index=True)
    if len(AllCMEs_daily.columns) == 0 or AllCMEs_daily.empty:
        raise CMENotfoundError("No CMEs crossed the line of sight on {}".format(obsdate))
    else:
        return AllCMEs, AllCMEs_daily, newt2, CenPA2, Width2, dmcme, dmsw, dmcme_df


def cmechaser(args):
    psr_a = get_source_coordinates(args)
    times, newt = get_times(args.cmecatalog, args.catsource)
    dmvars = get_dmvars(args.dmvarfile, args.solar_cutoff, psr_a)
    obsdates = Time(np.array(dmvars.dme.values), format='mjd').mjd
    psr_df = pd.DataFrame()
    y_max_quad = np.max(np.sin(np.deg2rad(dmvars.solarangle.values)))
    AllCMEs = pd.DataFrame()
    dmcme_df = pd.DataFrame()

    for obsdate in obsdates:

        color = plt.cm.viridis(plt.Normalize(min(obsdates), max(obsdates))(obsdate),
                               len(obsdates) / 12.)

        pa_psr, pa_sun = get_HelioPA(float(obsdate), psr_a)
        pa_psr_ang = Angle(pa_psr, u.deg)
        psr_df = pd.DataFrame.append(psr_df, [pa_psr_ang, pa_sun])
        mdlos = np.mean(np.sin(np.deg2rad(dmvars.solarangle[dmvars.dme == obsdate].values)))
        if args.catsource == 'CDAW':
            cmes_in_date, anticlock_limit, \
            clockwise_limit = get_cdaw_cmesindate_properties(times, newt, obsdate, mdlos)
        elif args.catsource == 'CACTUS':
            cmes_in_date, anticlock_limit, \
            clockwise_limit = get_cactus_cmesindate_properties(times, newt, obsdate, mdlos)

        try:
            AllCMEs, AllCMEs_daily, \
            newt2, CenPA2, Width2, \
            dmcme, dmsw, dmcme_df = allcmes(cmes_in_date, pa_psr_ang, obsdate, anticlock_limit, clockwise_limit, mdlos,
                                            AllCMEs, dmcme_df)
            quad_plot(y_max_quad, CenPA2, Width2, mdlos, color, pa_psr_ang, obsdate, args.psrname, AllCMEs_daily)
            AllCMEs_daily = AllCMEs_daily.replace(360.0, 'Halo', regex=True)
            for cmedate, cmepa in zip(AllCMEs_daily.Date_Time.values, AllCMEs_daily.CentralPA.values):
                if not (cmepa is None):
                    try:
                        star_list = zip([], [])
                        lasco_overlay(args.psrname, cmedate, pa_psr_ang, mdlos, cmepa, obsdate, star_list, psr_a, color,
                                      args)

                    except Exception as e:
                        logging.warning('LASCO plot failed!')
                        logging.exception(e)
        except CMENotfoundError:
            logging.warning("CMEs not encoutered for %s", obsdate)
            # AllCMEs = pd.DataFrame()

    if not (AllCMEs.empty):
        AllCMEs = AllCMEs.replace(360.0, 'Halo', regex=True)
        AllCMEs.drop_duplicates(inplace=True)
        AllCMEs.to_csv('{}_FinalCMElist.txt'.format(args.psrname), sep='\t', float_format='%g')
        try:
            dmcme_df.drop_duplicates(inplace=True)
            dmcme_means = dmcme_df.groupby('ObsDate').mean()
            dmcme_means.to_csv('{}_summed_dmcme_per_obs.txt'.format(args.psrname), sep='\t',
                               float_format='%g', index=False)
            dmcme_df.to_csv('{}_dmcme_per_obs.txt'.format(args.psrname), sep='\t', float_format='%g', index=False)
        except ValueError or KeyError as e:
            logging.exception("Saving CME DM list failed with error %s", e)
            pass
    else:
        logging.exception(CMENotfoundError("No CMEs crossed the line of sight on {}".format(obsdate)))
        pass


def main():
    setup_logger('CMEchaser', 'CMEchaser.log', level=logging.INFO)
    args = setup_and_parse_program_arguments()
    cmechaser(args)


if __name__ == '__main__':
    main()
