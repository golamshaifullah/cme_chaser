#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 25 09:41:33 2018

@author: shaifullah
"""

import os
import pycurl
import logging

logger = logging.getLogger('file_helpers')


def download_file(filename, path='/data/ancillary/attitude/predictive/', url='https://sohowww.nascom.nasa.gov'):
    """

    Parameters
    ----------
    filename
    path
    url

    Returns
    -------

    """
    with open(filename, "wb") as fp:
        try:
            curl = pycurl.Curl()
            curl.setopt(pycurl.URL, url + path + filename)
            curl.setopt(pycurl.WRITEDATA, fp)
            curl.perform()
            result = curl.getinfo(pycurl.HTTP_CODE)
        except Exception as e:
            logging.error('Cannot download file %s from %s', filename, url)
            logging.exception(e)
            raise SystemExit(1)
        finally:
            curl.close()
        if result == 404:
            os.remove(filename)
            raise pycurl.error('{} not found'.format(filename))
        return result


def stepinandoutofdir(dirname):
    """

    Parameters
    ----------
    dirname

    Returns
    -------

    """

    def decorator(func):
        def wrapper(*args, **kwargs):
            owd = os.getcwd()
            os.mkdirs(dirname, exists_ok=True)
            os.chdir(dirname)
            try:
                func(*args, **kwargs)
            finally:
                os.chdir(owd)

        return wrapper

    return decorator

def main():
    """
    TOD: Write a useful main function
    Returns
    -------

    """


if __name__ == "__main__":
    main()
